package sqltest;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;


public class cryptTest {
	
	public static boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
 }

	public static void main(String[] args) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, IOException, InvalidKeySpecException {
		// TODO Auto-generated method stub
		
		DESKeySpec keySpec = new DESKeySpec("DBConn Oida".getBytes("UTF8"));
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
		SecretKey key = keyFactory.generateSecret(keySpec);
		sun.misc.BASE64Encoder base64encoder = new BASE64Encoder();
		sun.misc.BASE64Decoder base64decoder = new BASE64Decoder();
		

		// ENCODE plainTextPassword String
		String plainTextPassword = "rootpw";
		byte[] cleartext = plainTextPassword.getBytes("UTF8");      

		Cipher cipher = Cipher.getInstance("DES"); // cipher is not thread safe
		cipher.init(Cipher.ENCRYPT_MODE, key);
		String encryptedPwd = base64encoder.encode(cipher.doFinal(cleartext));
		System.out.println(encryptedPwd);
		// now you can store it 
		

		// DECODE encryptedPwd String
		byte[] encrypedPwdBytes = base64decoder.decodeBuffer(encryptedPwd);

		Cipher cipher1 = Cipher.getInstance("DES");// cipher is not thread safe
		cipher1.init(Cipher.DECRYPT_MODE, key);
		byte[] plainTextPwdBytes = (cipher1.doFinal(encrypedPwdBytes));
		String pwd = new String(plainTextPwdBytes);
		System.out.println(pwd);
		
		String mail = "rene.meilbeck@students.fhnw.ch";
		System.out.println(isValidEmailAddress(mail));
		
		

	}

}
