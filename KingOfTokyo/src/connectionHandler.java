package sqltest;

import java.util.prefs.Preferences;

public class connectionHandler {
  static Preferences preferences = 
      Preferences.userNodeForPackage(myConnection.class);

  public static void setCredentials(String username, String password) {
    preferences.put("db_username", username);
    preferences.put("db_password", password);
  }

  public static String getUsername() {
    return preferences.get("db_username", null);
  }

  public static String getPassword() {
    return preferences.get("db_password", null);
  }

  
}
